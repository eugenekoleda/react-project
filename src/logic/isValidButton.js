export default function isValidButton(btn) {
    return !isNaN(parseFloat(btn)) && isFinite(btn);
};