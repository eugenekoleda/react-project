export default function createUser(userName) {
    let user_id = parseInt(localStorage.getItem('id'));
    if (!user_id) {
        user_id = 0;
    }
    const newUser = {
        name: userName,
        id: user_id,
        btns: [],
        history: []
    };
    user_id++;
    localStorage.setItem('id', user_id);
    return newUser;
}