import findIdx from "./findIdx";


export default function getEditedUser(users, newName, userId) {
    const idx = findIdx(users, userId);
    const editedUser = users[idx];
    editedUser['name'] = newName;
    return editedUser;
}