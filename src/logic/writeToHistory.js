export default function writeToHistory(displaying, res, currentUser) {
    let history = currentUser.history;
    const calculation = displaying + '=' + res;
    if (history.length !== 0) {
        history = [...history, calculation];
    } else {
        history = [calculation];
    }
    currentUser.history = history;
    return currentUser;
};
