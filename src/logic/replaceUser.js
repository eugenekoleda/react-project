import findIdx from "./findIdx";


/*
* This function replaced user in array of users, set current user and new array of users into localStorage
* and return array of edited users for setting it into redux store
* */

/**
 * @param {Array} users: array of users that are objects
 * @param {Object} currentUser: object of current user
 * @returns {Array} array of edited users
 * */
export default function replaceUser(users, currentUser) {
    const idx = findIdx(users, currentUser.id);
    const editedUsers = [...users.slice(0, idx), currentUser,...users.slice(idx + 1)];
    localStorage.setItem('users', JSON.stringify(editedUsers));
    localStorage.setItem('current-user', JSON.stringify(currentUser));
    return editedUsers;
};
