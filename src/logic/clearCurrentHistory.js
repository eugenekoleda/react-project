export default function clearCurrentHistory(currentUser) {
    currentUser.history = [];
    return currentUser;
};
