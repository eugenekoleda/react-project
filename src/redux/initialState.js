export const initialState = {
    users: JSON.parse(localStorage.getItem('users')) === null ? [] : JSON.parse(localStorage.getItem('users')),
    currentUser: JSON.parse(localStorage.getItem('current-user')),
    currentBtn: localStorage.getItem('current-button') === null ? '' : localStorage.getItem('current-button'),
};
