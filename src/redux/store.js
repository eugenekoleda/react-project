import { initialState } from "../redux/initialState";
import { usersReducer } from '../reducers/reducers';
import { createStore } from 'redux';


export const store = createStore(usersReducer, initialState, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
