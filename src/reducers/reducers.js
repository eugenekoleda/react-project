import { initialState } from '../index'
import * as type from '../consts/actionTypes'
import findIdx from "../logic/findIdx";
import replaceUser from "../logic/replaceUser";

export function usersReducer(state=initialState, action) {
    switch (action.type) {
        case type.ADD_USER: {
            const users = [...state.users, action.user];
            localStorage.setItem('users', JSON.stringify(users));
            return {
                ...state,
                users,
            };
        }
        case type.CHOOSE_USER: {
            localStorage.setItem('current-user', JSON.stringify(action.user));
            return {
                ...state,
                currentUser: action.user,
                currentUserId: action.userId,
            };
        }
        case type.EDIT_USER: {
            console.log(action.editedUser.id);
            const users = replaceUser(state.users, action.editedUser);
            return {
                ...state,
                users,
            };
        }
        case type.DELETE_USER: {
            const idx = findIdx(state.users, action.userId);
            const users = [...state.users.slice(0, idx), ...state.users.slice(idx + 1)];
            localStorage.setItem('users', JSON.stringify(users));
            return {
                ...state,
                users,
            };
        }
        case type.WRITE_HISTORY: {
            const users = replaceUser(state.users, action.currentUser);
            return {
                ...state,
                users,
                currentUser: { ...action.currentUser },
            };
        }
        case type.CLEAR_HISTORY: {
            const users = replaceUser(state.users, action.currentUser);
            return {
                ...state,
                users,
                currentUser: { ...action.currentUser },
            };
        }
        case type.CHOOSE_BUTTON: {
            localStorage.setItem('current-button', action.btn);
            return {
                ...state,
                currentBtn: action.btn,
            };
        }
        case type.ADD_BUTTON: {
            const currentUser = state.currentUser;
            currentUser.btns.push(action.btn);
            const users = replaceUser(state.users, currentUser);
            return {
                ...state,
                users,
                currentUser: { ...currentUser },
            };
        }
        case type.DELETE_BUTTON: {
            let btns = state.currentUser.btns;
            const btnIdx = btns.findIndex((el) => el === state.currentBtn);
            btns = [...btns.slice(0, btnIdx), ...btns.slice(btnIdx + 1)];
            state.currentUser.btns = btns;
            const users = replaceUser(state.users, state.currentUser);
            return {
                ...state,
                users,
                currentUser: { ...state.currentUser },
            };
        }
        case type.EDIT_BUTTON: {
            let btns = state.currentUser.btns;
            const btnIdx = btns.findIndex((el) => el === state.currentBtn);
            btns[btnIdx] = action.newBtn;
            state.currentUser.btns = btns;
            const users = replaceUser(state.users, state.currentUser);
            return {
                ...state,
                users,
                currentUser: { ...state.currentUser },
            };
        }
        default:
            return state;
    }
}
