import React from 'react';


import {
    BrowserRouter,
    Route,
    Switch
} from 'react-router-dom';

import Calc from '../screens/Calc';
import Home from '../screens/Home';
import CreateUser from '../screens/CreateUser';
import BtnList from '../screens/BtnList';
import CreateBtn from '../screens/CreateBtn';
import {connect} from "react-redux";


const Router = ( { currentUser, currentBtn }) => {
    return(
        <BrowserRouter>
            <Switch>
                <Route exact path='/' component={Home} />
                <Route exact path='/calculator' component={Calc} />
                <Route exact path='/calculator/buttons' component={BtnList} />
                <Route path='/calculator/buttons/createbutton' render={(props) => <CreateBtn {...props} title = 'Create' button={ currentBtn }/>} />
                <Route path='/calculator/buttons/editbutton' render={(props) => <CreateBtn {...props} title = 'Edit' button={ currentBtn }/>} />
                <Route path='/createuser' render={(props) => <CreateUser {...props} title = 'Create' userName={''}/>} />
                <Route path='/edituser' render={(props) => <CreateUser {...props} title = 'Edit' userName={currentUser.name}/>} />
            </Switch>
        </BrowserRouter>
    )
};

const mapStateToProps = ({ currentUser, currentBtn }) => {
    return {
        currentUser,
        currentBtn,
    };
};

export default connect(mapStateToProps, null)(Router);
