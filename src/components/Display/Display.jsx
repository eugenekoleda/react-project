import React from "react";

import "./Display.css";

const Display = ({ input }) => {
  return (
    <div className="calc-display">
      <p>{input}</p>
    </div>
  );
};

export default Display;
