import React from "react";

import "./Button.css";

const Button = ({ props, el }) => {
  const { label, btnStyle, action, eventBtn } = props;
  return (
    <button
      type="button"
      className={btnStyle}
      onClick={() => eventBtn(label, action, el)}
    >
      {" "}
      {label}{" "}
    </button>
  );
};

export default Button;
