import React, { Component } from "react";
import Button from "../Button";

import "./History.css";

import clearCurrentHistory from "../../logic/clearCurrentHistory";
import { clearHistory } from "../../actions/actions";
import { connect } from "react-redux";

class History extends Component {
  state = {
    visible: false
  };

  showHistory = () => {
    const visible = this.state.visible;
    this.setState({ visible: !visible });
  };

  clearHistory = () => {
    const currentUser = clearCurrentHistory(this.props.currentUser);
    this.props.clearHistory(currentUser);
  };

  render() {
    const history = this.props.currentUser.history;
    const historyList = history.map(el => <p key={el}>{el}</p>);
    const clearHistoryBtn = {
      label: "Clear",
      action: "Clear",
      btnStyle: "clear-history",
      eventBtn: this.clearHistory
    };
    const showHistory = {
      label: this.state.visible ? "˄" : "˅",
      action: "History",
      btnStyle: "show-history",
      eventBtn: this.showHistory
    };
    if (this.state.visible) {
      return (
        <div className="history-block">
          <Button props={showHistory} />
          <div className="history-list">
            <Button props={clearHistoryBtn} />
            {historyList}
          </div>
        </div>
      );
    } else {
      return <Button props={showHistory} />;
    }
  }
}

const mapStateToProps = ({ currentUser }) => {
  return {
    currentUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    clearHistory: user => {
      dispatch(clearHistory(user));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(History);
