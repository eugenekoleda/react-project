import React from "react";

import "./FullName.css";
import { connect } from "react-redux";

const FullName = ({ currentUser }) => {
  return (
    <div className="full-name">
      <p>{currentUser.name}</p>
    </div>
  );
};

const mapStateToProps = ({ currentUser }) => {
  return {
    currentUser
  };
};

export default connect(
  mapStateToProps,
  null
)(FullName);
