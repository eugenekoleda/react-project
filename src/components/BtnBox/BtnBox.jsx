import React from "react";

import Button from "../Button";

import "./BtnBox.css";

const BtnBox = ({ add, clear, result, sqrt, customBtns }) => {
  const config = [
    {
      label: "C",
      btnStyle: "resetBtn",
      action: "clear_all",
      eventBtn: clear
    },
    {
      label: "←",
      btnStyle: "resetBtn",
      action: "clear_symbol",
      eventBtn: clear
    },
    {
      label: "√",
      btnStyle: "baseBtn",
      action: "sqrt",
      eventBtn: sqrt
    },
    {
      label: "÷",
      btnStyle: "baseBtn",
      action: "/",
      eventBtn: add
    },
    {
      label: "7",
      btnStyle: "numsBtn",
      action: "7",
      eventBtn: add
    },
    {
      label: "8",
      btnStyle: "numsBtn",
      action: "8",
      eventBtn: add
    },
    {
      label: "9",
      btnStyle: "numsBtn",
      action: "9",
      eventBtn: add
    },
    {
      label: "x",
      btnStyle: "baseBtn",
      action: "*",
      eventBtn: add
    },
    {
      label: "4",
      btnStyle: "numsBtn",
      action: "4",
      eventBtn: add
    },
    {
      label: "5",
      btnStyle: "numsBtn",
      action: "5",
      eventBtn: add
    },
    {
      label: "6",
      btnStyle: "numsBtn",
      action: "6",
      eventBtn: add
    },
    {
      label: "-",
      btnStyle: "baseBtn",
      action: "-",
      eventBtn: add
    },
    {
      label: "1",
      btnStyle: "numsBtn",
      action: "1",
      eventBtn: add
    },
    {
      label: "2",
      btnStyle: "numsBtn",
      action: "2",
      eventBtn: add
    },
    {
      label: "3",
      btnStyle: "numsBtn",
      action: "3",
      eventBtn: add
    },
    {
      label: "+",
      btnStyle: "baseBtn",
      action: "+",
      eventBtn: add
    },
    {
      label: "%",
      btnStyle: "baseBtn",
      action: "%",
      eventBtn: add
    },
    {
      label: "0",
      btnStyle: "numsBtn",
      action: "0",
      eventBtn: add
    },
    {
      label: ".",
      btnStyle: "baseBtn",
      action: ".",
      eventBtn: add
    },
    {
      label: "=",
      btnStyle: "resultBtn",
      action: "=",
      eventBtn: result
    }
  ];
  const btns = config.map(el => <Button key={el.label} props={el} />);
  if (customBtns.length) {
    customBtns = customBtns.map(el => {
      const properties = {
        label: el,
        btnStyle: "baseBtn",
        action: el,
        eventBtn: add
      };
      return <Button key={el} props={properties} />;
    });
  } else {
    customBtns = [];
  }

  return (
    <div className="btn-box">
      {btns}
      {customBtns}
    </div>
  );
};

export default BtnBox;
