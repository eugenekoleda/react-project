// action types
export const ADD_USER = 'ADD_USER';
export const DELETE_USER = 'DELETE_USER';
export const EDIT_USER = 'EDIT_USER';
export const ADD_BUTTON = 'ADD_BUTTON';
export const DELETE_BUTTON = 'DELETE_BUTTON';
export const EDIT_BUTTON = 'EDIT_BUTTON';
export const WRITE_HISTORY = 'WRITE_HISTORY';
export const CLEAR_HISTORY = 'CLEAR_HISTORY';
export const CHOOSE_USER = 'CHOOSE_USER';
export const CHOOSE_BUTTON = 'CHOOSE_BUTTON';