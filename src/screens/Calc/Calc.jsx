import React, { Component } from "react";
import Display from "../../components/Display";
import BtnBox from "../../components/BtnBox";
import FullName from "../../components/FullName";
import History from "../../components/History";

import { Link } from "react-router-dom";

import "./Calc.css";
import { connect } from "react-redux";

import writeToHistory from "../../logic/writeToHistory";
import { writeHistory } from "../../actions/actions";

class Calc extends Component {
  state = {
    input: "",
    displaying: ""
  };
  operations = ["+", "-", "*", "/", "%"];

  clearAll = () => this.setState({ input: "", displaying: "" });

  deleteSymbol = () => {
    let new_input = this.state.input.slice(0, this.state.input.length - 1);
    let new_displaying = this.state.displaying.slice(
      0,
      this.state.displaying.length - 1
    );
    this.setState({
      input: new_input,
      displaying: new_displaying
    });
  };

  isInvalidDisplaying = displaying =>
    displaying === "Division by zero is impossible" ||
    displaying === "Result is undefined";

  addToInput = (val, action) => {
    if (
      this.operations.includes(action) &&
      this.operations.includes(this.state.input[this.state.input.length - 1])
    ) {
      this.deleteSymbol();
    }
    this.setState(({ input, displaying }) => {
      if (
        this.isInvalidDisplaying(displaying) &&
        this.operations.includes(action)
      ) {
        displaying = "0";
        input = "0";
      } else if (
        this.isInvalidDisplaying(displaying) &&
        !this.operations.includes(action)
      ) {
        displaying = "";
        input = "";
      }
      return {
        input: input + action,
        displaying: displaying + val
      };
    });
  };

  clearInput = (val, action) => {
    if (action === "clear_all") {
      this.clearAll();
    } else {
      this.deleteSymbol();
    }
  };

  getSqrt = () => {
    let sqrt = Math.sqrt(this.state.input);
    this.setState({
      displaying: sqrt,
      input: sqrt
    });
  };

  calculatePercent = symbol => {
    let nums = this.state.input.split(symbol);
    nums[1] = nums[1].slice(0, nums[1].length - 1);
    nums = nums.map(el => parseFloat(el));
    switch (symbol) {
      case "+":
        return nums[0] + (nums[1] / 100) * nums[0];
      case "-":
        return nums[0] - (nums[1] / 100) * nums[0];
      case "*":
        return nums[0] * (nums[1] / 100) * nums[0];
      case "/":
        return (nums[0] / (nums[1] / 100)) * nums[0];
      default:
        return;
    }
  };

  getPercent = () => {
    let res;
    if (this.state.input.includes("+")) {
      res = this.calculatePercent("+");
    } else if (this.state.input.includes("-")) {
      res = this.calculatePercent("-");
    } else if (this.state.input.includes("*")) {
      res = this.calculatePercent("*");
    } else if (this.state.input.includes("/")) {
      res = this.calculatePercent("/");
    }
    return res;
  };

  getResult = () => {
    let res;
    if (this.state.input.includes("/0")) {
      res = "Division by zero is impossible";
    } else if (this.state.input.includes("%")) {
      res = this.getPercent();
    } else {
      res = eval(this.state.input);
      if (isNaN(res)) {
        res = "Result is undefined";
      }
    }
    const currentUser = writeToHistory(
      this.state.displaying,
      res,
      this.props.currentUser
    );
    this.props.writeHistory(currentUser);
    this.setState({
      displaying: res,
      input: res
    });
  };

  render() {
    return (
      <div className="app">
        <FullName />
        <Link to="/" className="change-user-btn">
          Change user
        </Link>
        <div className="calculator-block">
          <div className="calculator">
            <Display input={this.state.displaying} />
            <div className="history">
              <History />
            </div>
            <BtnBox
              add={this.addToInput}
              clear={this.clearInput}
              result={this.getResult}
              sqrt={this.getSqrt}
              customBtns={this.props.currentUser.btns}
            />
          </div>
        </div>
        <Link to="/calculator/buttons" className="btns-list">
          List of buttons
        </Link>
      </div>
    );
  }
}

const mapStateToProps = ({ currentUser }) => {
  return {
    currentUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    writeHistory: user => {
      dispatch(writeHistory(user));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Calc);
