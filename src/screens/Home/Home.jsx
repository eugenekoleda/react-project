import React from "react";
import Button from "../../components/Button";

import { Link } from "react-router-dom";

import { chooseUser, deleteUser } from "../../actions/actions";

import "./Home.css";
import { connect } from "react-redux";

const Home = props => {
  const { deleteUser, chooseUser, users, currentUser } = props;
  const delUser = (label, action, el) => {
    deleteUser(el.id);
  };

  const del_btn = {
    label: "Delete",
    btnStyle: "del-btn",
    eventBtn: delUser
  };
  const usersList = users.map(el => {
    return (
      <li key={el["name"]}>
        <Link
          to="/calculator"
          onClick={() => {
            chooseUser(el);
          }}
          className={el.id === currentUser.id ? "current-user" : "base-user"}
        >
          {el["name"]}
        </Link>
        <Link
          to="/edituser"
          className="edit-btn"
          onClick={() => {
            chooseUser(el);
          }}
        >
          Edit
        </Link>
        <Button props={del_btn} el={el} />
      </li>
    );
  });
  return (
    <div className="main">
      <Link to="/createuser" className="add-btn">
        + Add
      </Link>
      <ul className="user-list">{usersList}</ul>
    </div>
  );
};

const mapStateToProps = ({ users, currentUser }) => {
  return {
    users,
    currentUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    deleteUser: users => {
      dispatch(deleteUser(users));
    },
    chooseUser: (user, userId) => {
      dispatch(chooseUser(user, userId));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
