import React from "react";
import Button from "../../components/Button";

import { Link } from "react-router-dom";

import "../Home/Home.css";
import "./BtnList.css";
import { chooseButton, deleteButton } from "../../actions/actions";
import { connect } from "react-redux";

const BtnList = props => {
  const { deleteButton, chooseButton } = props;

  const delBtn = (label, action, el) => {
    chooseButton(el);
    deleteButton();
  };

  const del_btn = {
    label: "Delete",
    btnStyle: "del-btn",
    eventBtn: delBtn
  };
  let buttonsList = [];
  if (props.currentUser.btns.length > 0) {
    buttonsList = props.currentUser.btns.map(el => {
      return (
        <li key={el}>
          <div className="button-value">{el}</div>
          <Link
            to="/calculator/buttons/editbutton"
            onClick={() => chooseButton(el)}
            className="edit-btn"
          >
            Edit
          </Link>
          <Button props={del_btn} el={el} />
        </li>
      );
    });
  }
  return (
    <div className="buttons">
      <Link
        to="/calculator/buttons/createbutton"
        onClick={() => chooseButton("")}
        className="add-btn"
      >
        + Add
      </Link>
      <Link to="/calculator" className="back-btn">
        &larr; Back
      </Link>
      <ul className="buttons-list">{buttonsList}</ul>
    </div>
  );
};

const mapStateToProps = ({ currentUser }) => {
  return {
    currentUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    deleteButton: () => {
      dispatch(deleteButton());
    },
    chooseButton: btn => {
      dispatch(chooseButton(btn));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BtnList);
