import React, { Component } from "react";
import { Redirect } from "react-router-dom";

import "./CreateUser.css";
import createUser from "../../logic/createUser";
import getEditedUser from "../../logic/editUser";

import { addUser, editUser } from "../../actions/actions";
import { connect } from "react-redux";
import isValidName from "../../logic/isValidName";

class CreateUser extends Component {
  state = {
    label: this.props.userName,
    redirect: false
  };

  onLabelChange = event => {
    this.setState({
      label: event.target.value
    });
  };

  onSubmit = event => {
    event.preventDefault();
    if (!isValidName(this.state.label)) {
      alert("Please enter valid name of user");
      return;
    }
    let users = this.props.users;
    if (!users) {
      users = [];
    }
    if (this.props.title === "Create") {
      const newUser = createUser(this.state.label);
      this.props.addUser(newUser);
    } else {
      const editedUser = getEditedUser(
        users,
        this.state.label,
        this.props.currentUser.id
      );
      this.props.editUser(editedUser);
    }
    this.setState({ redirect: true });
  };

  render() {
    if (this.state.redirect) {
      return <Redirect to="/" />;
    }
    return (
      <div className="create-user-surface">
        <div className="create-user">
          <h1>{this.props.title} User</h1>
          <form onSubmit={this.onSubmit} className="create-user-form">
            <input
              type="text"
              placeholder="put name of user here..."
              onChange={this.onLabelChange}
              value={this.state.label}
            />
            <button type="submit">{this.props.title}</button>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ users, currentUser }) => {
  return {
    users,
    currentUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addUser: users => {
      dispatch(addUser(users));
    },
    editUser: users => {
      dispatch(editUser(users));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateUser);
