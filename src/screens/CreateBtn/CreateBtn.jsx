import React, { Component } from "react";
import { Redirect } from "react-router-dom";

import "./CreateBtn.css";
import { addButton, chooseButton, editButton } from "../../actions/actions";
import { connect } from "react-redux";
import isValidButton from "../../logic/isValidButton";

class CreateBtn extends Component {
  state = {
    label: this.props.button,
    redirect: false
  };

  onLabelChange = event => {
    this.setState({
      label: event.target.value
    });
  };

  onSubmit = event => {
    event.preventDefault();
    if (!isValidButton(this.state.label)) {
      alert("Please enter valid value of button");
      return;
    }
    if (this.props.currentUser.btns.includes(this.state.label)) {
      alert("This button already exists");
      return;
    }
    if (this.props.title === "Create") {
      this.props.addButton(this.state.label);
    } else {
      this.props.chooseButton(this.props.button);
      this.props.editButton(this.state.label);
    }
    this.setState({ redirect: true });
  };

  render() {
    if (this.state.redirect) {
      return <Redirect to="/calculator" />;
    }
    return (
      <div className="create-btn-surface">
        <div className="create-button">
          <h1>{this.props.title} Button</h1>
          <form onSubmit={this.onSubmit} className="create-button-form">
            <input
              type="text"
              placeholder="put value of button here..."
              onChange={this.onLabelChange}
              value={this.state.label}
            />
            <button type="submit">{this.props.title}</button>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ currentUser }) => {
  return {
    currentUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addButton: btn => {
      dispatch(addButton(btn));
    },
    chooseButton: btn => {
      dispatch(chooseButton(btn));
    },
    editButton: btn => {
      dispatch(editButton(btn));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateBtn);
