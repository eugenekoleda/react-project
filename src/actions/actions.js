//action creators
import * as type from '../consts/actionTypes'

export function addUser(user) {
    return {
        type: type.ADD_USER,
        user,
    };
}

export function deleteUser(userId) {
    return {
        type: type.DELETE_USER,
        userId,
    };
}

export function editUser(editedUser) {
    return {
        type: type.EDIT_USER,
        editedUser,
    };
}

export function addButton(btn) {
    return {
        type: type.ADD_BUTTON,
        btn,
    };
}

export function deleteButton() {
    return {
        type: type.DELETE_BUTTON,
    };
}

export function editButton(newBtn) {
    return {
        type: type.EDIT_BUTTON,
        newBtn,
    };
}

export function writeHistory(currentUser) {
    return {
        type: type.WRITE_HISTORY,
        currentUser,
    };
}

export function clearHistory(currentUser) {
    return {
        type: type.CLEAR_HISTORY,
        currentUser,
    };
}

export function chooseUser(user, userId) {
    return {
        type: type.CHOOSE_USER,
        user,
        userId,
    };
}

export function chooseButton(btn) {
    return {
        type: type.CHOOSE_BUTTON,
        btn
    };
}
